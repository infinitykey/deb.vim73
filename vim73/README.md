Built on VMware:  Linux debian 2.6.32-5-amd64 #1 SMP Mon Feb 25 00:26:11 UTC 2013 x86_64 GNU/Linux<br />
lua 5.1.4<br />
python 2.6.6<br />
python 3.1.3<br />
ruby 1.8<br />
tclsh 8.4<br />

./configure --enable-luainterp --enable-perlinterp --enable-pythoninterp --enable-python3interp --enable-tclinterp --enable-rubyinterp --enable-cscope --enable-gui=auto --enable-multibyte --enable-gnome-check --with-compiledby=johnEsacks

make

checkinstall
