debs
====

Debian packages (.deb) for custom builds for reuse across my different systems without having the need for all the dev tools.

.deb build instructions:

Step 1:
sudo apt-get install build-essential automake autoconf libtool pkg-config libcurl4-openssl-dev intltool libxml2-dev libgtk2.0-dev libnotify-dev libglib2.0-dev libevent-dev checkinstall

Step 2: 
extract package src && cd to dir

Step 3:
./configure <options> && make

Step 4:
sudo checkinstall
(autoinstalls & creates .deb)

All configure options built with are in the subsiquent directories.



